<?php

/*
 * Implement hook_theme()
 */

function avatars_theme($existing, $type, $theme, $path) {
  return array(
      'avatars' => array(
          'variables' => array('accounts' => array()),
      ),
  );
}

/*
 * Implement hook_block_info()
 */

function avatars_block_info() {
  $blocks['recently_active_users'] = array(
      'info' => t('Recently active users'),
      'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  $blocks['recently_registered_users'] = array(
      'info' => t('Recently registered users'),
      'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  $blocks['most_active_users'] = array(
      'info' => t('Most active users'),
      'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function avatars_block_configure($delta = '') {
  $form = array();
  $count = array(
      5 => 5,
      8 => 8,
      10 => 10,
      20 => 20,
      30 => 30,
      40 => 40,
  );
  switch ($delta):
    case 'recently_active_users':
      $form['recently_active_users_count'] = array(
          '#type' => 'select',
          '#title' => t('Count'),
          '#options' => $count,
          '#default_value' => variable_get('recently_active_users_count', 40),
          '#description' => t('Set number of avatars to display.'),
      );
      break;
    case 'recently_registered_users':
      $form['recently_registered_users_count'] = array(
          '#type' => 'select',
          '#title' => t('Count'),
          '#options' => $count,
          '#default_value' => variable_get('recently_registered_users_count', 40),
          '#description' => t('Set number of avatars to display.'),
      );
      break;
    case 'most_active_users':
      $form['most_active_users_count'] = array(
          '#type' => 'select',
          '#title' => t('Count'),
          '#options' => $count,
          '#default_value' => variable_get('most_active_users_count', 40),
          '#description' => t('Set number of avatars to display.'),
      );
      break;
  endswitch;
  return $form;
}

/**
 * Implements hook_block_save().
 */
function avatars_block_save($delta = '', $edit = array()) {
  switch ($delta):
    case 'recently_active_users':
      variable_set('recently_active_users_count', $edit['recently_active_users_count']);
      break;
    case 'recently_registered_users':
      variable_set('recently_registered_users_count', $edit['recently_registered_users_count']);
      break;
    case 'most_active_users':
      variable_set('most_active_users_count', $edit['most_active_users_count']);
      break;
  endswitch;
  return;
}

function avatars_block_view($delta = '') {
  $block = array();
  switch ($delta):
    case 'recently_active_users':
      $block['subject'] = t('Recently active users');
      $accounts = _avatar_recently_active_users();
      $block['content'] = array(
          '#theme' => 'avatars',
          '#accounts' => $accounts
      );
      break;
    case 'recently_registered_users':
      $block['subject'] = t('Recently registered users');
      $accounts = _avatar_recently_registered_users();
      $block['content'] = array(
          '#theme' => 'avatars',
          '#accounts' => $accounts
      );
      break;
    case 'most_active_users':
      $block['subject'] = t('Most active users');
      $accounts = _avatar_most_active_users();
      $block['content'] = array(
          '#theme' => 'avatars',
          '#accounts' => $accounts
      );
      break;
  endswitch;
  return $block;
}

function theme_avatars($variables) {
  $accounts = $variables['accounts'];
  $items = array();
  foreach ($accounts as $account) {
    $items['user-' . $account->uid] = array(
        'title' => theme('user_picture', array('account' => $account)),
        'html' => TRUE,
    );
  }
  $render = array(
      '#theme' => 'links',
      '#links' => $items,
      '#attributes' => array(
          'class' => array('clearfix')
      )
  );
  return drupal_render($render);
}

function _avatar_recently_active_users() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'user')
          ->propertyCondition('status', 1)
          ->propertyOrderBy('access', 'DESC')
          ->range(0, (int) variable_get('recently_active_users_count', 40));
  $result = $query->execute();
  $accounts = array();
  if (isset($result['user'])) {
    $uids = array_keys($result['user']);
    $accounts = entity_load('user', $uids);
  }

  return $accounts;
}

function _avatar_recently_registered_users() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'user')
          ->propertyCondition('status', 1)
          ->propertyOrderBy('created', 'DESC')
          ->range(0, (int) variable_get('recently_registered_users_count', 40));
  $result = $query->execute();
  $accounts = array();
  if (isset($result['user'])) {
    $uids = array_keys($result['user']);
    $accounts = entity_load('user', $uids);
  }

  return $accounts;
}

function _avatar_most_active_users() {
  $query = db_select('users', 'u');
  $query->join('node', 'n', 'u.uid = n.uid');
  $query->condition('u.status', 1)
          ->condition('n.status', 1)
          ->groupBy('n.uid')
          ->fields('u', array('uid'))
          ->orderBy('count', 'DESC')
          ->range(0, (int) variable_get('most_active_users_count', 40))
          ->addExpression('count(n.nid)', 'count');

  $result = $query->execute();
  $accounts = array();
  while ($account = $result->fetchObject()) {
    $accounts[] = $account;
  }

  return $accounts;
}